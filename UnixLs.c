/*
* links referenced :
*https ://askubuntu.com/questions/783586/how-does-ls-l-format-its-output-so-neatly
*https ://stackoverflow.com/questions/10323060/printing-file-permissions-like-ls-l-using-stat2-in-c
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include <time.h>
#include <stdlib.h>

typedef enum { 
	ft_dir, 
	ft_file 
} FileType; 

void get_directory_info(char** argv, int argc, int numSwitches, int casei, int casel);

void get_switch_info(char* dirName, int casei, int casel);

void print_info(int casei, int casel, struct dirent* dp, struct stat* buf, FileType ft, char* dirName);

int main(int argc, char* argv[]) {
	int opt;
	extern char* optarg;
	char** args;
	extern int optind;
	int casei = 0;
	int casel = 0;
	int numSwitches = 0;
	int i = 0;
	int switchOccured = 0; 
	int parameterOccured = 0; 
	int noSwitches = 1; 

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-l") == 0 || strcmp(argv[i], "-i") == 0 || strcmp(argv[i], "-il") == 0 || strcmp(argv[i], "-li") == 0) {
			switchOccured = 1;
			noSwitches = 0;
			if (parameterOccured && switchOccured) {
				printf("parameters must come after all switches\n");
				exit(1);
			}
		} 
		else {
			parameterOccured = 1; 
			if (switchOccured == 0 && noSwitches == 0) {
				printf("parameters must come after switches\n");
				exit(1);
			}
		}
	}

	while ((opt = getopt(argc, argv, "li")) != -1) {
		switch (opt) {
		case 'l':
			casel = 1;
			numSwitches = 1;
			break;
		case 'i':
			casei = 1;
			numSwitches = 1;
			break;
		case '?':
			break;
		}
		i++;
	}

	for (i = 1; i < numSwitches; i++) {
		if (strcmp(argv[i], "-l") != 0 && strcmp(argv[i], "-i") != 0 && strcmp(argv[i], "-il") != 0 && strcmp(argv[i], "-li") != 0) {
			printf("ERROR: switches must come before directories\n");
			printf("argv[%d] %s\n", i, argv[i]);
		}
	}

	get_directory_info(argv, argc, numSwitches, casei, casel);

	return 0;
}

void get_directory_info(char** argv, int argc, int numSwitches, int casei, int casel) {
	int i = 0;
	struct stat dirCheck;

	if (numSwitches < argc - 1) {
		for (i = numSwitches + 1; i < argc; i++) {
			if (stat(argv[i], &dirCheck) == 0) {
				if (dirCheck.st_mode & S_IFDIR) {
					printf("\n%s:\n", argv[i]);
				}
			}
			get_switch_info(argv[i], casei, casel);
		}
	}
	else {
		get_switch_info("./", casei, casel);
	}
}

void get_switch_info(char* dirName, int casei, int casel) {
	DIR* dir;
	struct dirent* dp;
	struct stat buf;
	struct passwd* pw = NULL;
	struct group* grp;
	char* currTime;
	char timeBuf[80];
	struct tm* timeInfo;

	dir = opendir(dirName);
	if (dir == NULL) {
		if (lstat(dirName, &buf) == 0) {
			print_info(casei, casel, dp, &buf, ft_file, dirName);
			printf("%s\n", dirName);
		} 
		else {
			printf("ERROR: failed to open directory %s\n", dirName);
		}
	}
	else {
		while ((dp = readdir(dir)) != NULL) {
			if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && (dp->d_name[0] != '.')) {
				print_info(casei, casel, dp, &buf, ft_dir, dirName);
				printf("%s\n", dp->d_name);
			}
		}
		printf("\n");
	}
	closedir(dir);
}

void print_info(int casei, int casel, struct dirent* dp, struct stat* buf, FileType ft, char* dirName) {
	struct passwd* pw = NULL;
	struct group* grp;
	char* currTime;
	char timeBuf[80];
	struct tm* timeInfo;

	if (casei) {
		if (ft == ft_file) {
			printf("%20lu ", buf->st_ino);
		}
		else {
			printf("%20lu ", dp->d_ino);
		}
	}

	if (casel) {
		if (ft == ft_dir) {
			if (dp->d_type == DT_LNK) {
				lstat(dp->d_name, buf);
			}
			else {

			char* temp = malloc(sizeof(char) * (strlen(dirName) + strlen(dp->d_name)));
			strcpy(temp, dirName); 
			int length = strlen(temp) - 1; 
			if (temp[length] != '/') {
				strcat(temp, "/"); 
			}
			strcat(temp, dp->d_name); 

			if (stat(temp, buf) != 0) {
				perror("stat");
				return; 
			}
			free(temp);
			}
		}

		pw = getpwuid(buf->st_uid);
		grp = getgrgid(buf->st_gid);
		timeInfo = localtime(&(buf->st_mtime));
		strftime(timeBuf, 80, "%b %d %Y %H:%M", timeInfo);

		printf((S_ISDIR(buf->st_mode)) ? "d" : "-");
		printf((buf->st_mode & S_IRUSR) ? "r" : "-");
		printf((buf->st_mode & S_IWUSR) ? "w" : "-");
		printf((buf->st_mode & S_IXUSR) ? "x" : "-");
		printf((buf->st_mode & S_IRGRP) ? "r" : "-");
		printf((buf->st_mode & S_IWGRP) ? "w" : "-");
		printf((buf->st_mode & S_IXGRP) ? "x" : "-");
		printf((buf->st_mode & S_IROTH) ? "r" : "-");
		printf((buf->st_mode & S_IWOTH) ? "w" : "-");
		printf((buf->st_mode & S_IXOTH) ? "x" : "-");
		
		printf(" %lu %s %s %6li %s ", buf->st_nlink, pw->pw_name, grp->gr_name, buf->st_size, timeBuf);
	}
}